#include <stdio.h>
#include <string.h>
#include "RakNet/RakPeerInterface.h"
#include "RakNet/MessageIdentifiers.h"
#include "RakNet/BitStream.h"
#include "RakNet/RakNetTypes.h"  // MessageID

enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	ID_GAME_MESSAGE_2
	//****TO-DO
	//Add Message delivery
	//Add Message request
	//Add Username message
};

#pragma pack(push, 1)
struct PacketStruct
{
	unsigned char typeId; // Your type here
	
	char str[511];
};
#pragma pack(pop)


//****TO-DO
//Add message rquest
//Add message delivery
//Add username & IP address struct for host

int main(void)
{
	RakNet::Packet* packet;

	unsigned int maxClients = 10;
	unsigned short serverPort = 60000;
		
	char str[512];
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	bool isServer;

	//****TO-DO
	//UserStruct users[10]
	//char username[24]

	//****TO-DO
	//Lobby state:
	/* Welcome Zac's fablous chat room
	*  It's like Facebook but worse! :D
	 //Wait 1 seconds
	 //Clear screen
	* User Name: (No spaces!)
	 //read
	* Do you want to (H)ost a chat or (J)oin and existing one?
	//Host
	* Hosting chat
	* Enter Max Clients:
	* 
	//Run chat

	//Join existing
	* Enter IP
	//Sends message to join server

	*/ 

	//Command Message:
	/* 
	* Hello! Welcome to <Host Username>'s chatroom
	* If you want to send a private message type '/' then the username of the user you want to send to, followed by a space

	*/


	printf("(C) or (S)erver?\n");
	fgets(str, 512, stdin);
	if ((str[0] == 'c') || (str[0] == 'C'))
	{
		printf("Running as Client\n");

		printf("port number:\n");
		//Input port number (serverPort)
		fgets(str, 512, stdin);
		serverPort = std::atoi(str);

		RakNet::SocketDescriptor sd;
		peer->Startup(1, &sd, 1);
		isServer = false;
	}
	else 
	{
		printf("Running as Server\n");

		printf("port number:\n");
		//Input port number (serverPort)
		fgets(str, 512, stdin);
		serverPort = std::atoi(str);


		printf("max clients:\n");
		//Input max clients (maxClients)
		fgets(str, 512, stdin);
		maxClients = std::atoi(str);

		RakNet::SocketDescriptor sd(serverPort, 0);
		peer->Startup(maxClients, &sd, 1);
		isServer = true;
	}


	if (isServer)
	{
		printf("Starting the server.\n");
		// We need to let the server accept incoming connections from the clients
		peer->SetMaximumIncomingConnections(maxClients);
	}
	else {
		printf("Enter server IP or hit enter for 127.0.0.1\n");
		fgets(str, 512, stdin);
		//if (str[0] == 0)
		if (str[0] == '\n')
		{
			strcpy(str, "127.0.0.1");
		}
		printf("Starting the client.\n");
		peer->Connect(str, serverPort, 0, 0);

	}

	while (1)
	{
		//****TO-DO
		//Add message reading prompt

		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				printf("Another client has disconnected.\n");
				break;
			case ID_REMOTE_CONNECTION_LOST:
				printf("Another client has lost the connection.\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				printf("Another client has connected.\n");
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Our connection request has been accepted.\n");

				//****TO-DO
				//Send Username Message to Server

				PacketStruct msg;

				msg.typeId = (RakNet::MessageID)ID_GAME_MESSAGE_1;
				strcpy(msg.str, "Hello world");

				peer->Send((const char *)&msg,sizeof(PacketStruct), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				//peer->Send()
			}
				break;
			case ID_NEW_INCOMING_CONNECTION:
				printf("A connection is incoming.\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("The server is full.\n");
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				if (isServer) {
					printf("A client has disconnected.\n");
					//****TO-DO
					//Send Client-Disconnected message to all clients
				}
				else {
					printf("We have been disconnected.\n");
				}
				break;
			case ID_CONNECTION_LOST:
				if (isServer) {
					printf("A client lost the connection.\n");
					//****TO-DO
					//Send Client-Disconnected message to all clients
				}
				else {
					printf("Connection lost.\n");
				}
				break;
			case ID_GAME_MESSAGE_1:
			{
				const PacketStruct *msgIn = (PacketStruct*)packet->data;
				printf("%s\n", msgIn->str);
			
				PacketStruct msgOut;

				msgOut.typeId = (RakNet::MessageID)ID_GAME_MESSAGE_2;
				strcpy(msgOut.str, "Goodbye");

				peer->Send((const char*)&msgOut, sizeof(PacketStruct), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
				break;
			case ID_GAME_MESSAGE_2:
			{
				const PacketStruct* msgIn = (PacketStruct*)packet->data;
				printf("%s\n", msgIn->str);

				PacketStruct msgOut;

				msgOut.typeId = (RakNet::MessageID)ID_GAME_MESSAGE_1;
				strcpy(msgOut.str, "Hello");

				peer->Send((const char*)&msgOut, sizeof(PacketStruct), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
			break;
			//****TO-DO
			//Message request
			//Sent from client
			//Check for any null characters or anything code breaking (usernames for spaces)
			//Check if public/private
			//Read username if private
			//Creat message delivery command for clients
			
			//Message delivery
			//Sent from host
			//Display message: 
			/*
			*<Username> <Is private>: <Message>
			Ex: if private
			Chazix whispers: Hey looser

			Ex if public
			Chazix: You're the best!
			*/

			//Username
			//Check to make sure the username exists
			//Check the username for valid characters
			//Add username and IP address to array
			default:
				printf("Message with identifier %i has arrived.\n", packet->data[0]);
				break;
			}
		}
	}


	RakNet::RakPeerInterface::DestroyInstance(peer);

	return 0;
}
